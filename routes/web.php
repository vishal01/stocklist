<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'LoginController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('listcompanies', 'StockController@stocklist')->name('listcompanies');
Route::get('companydata', 'StockController@get_company')->name('companydata');



