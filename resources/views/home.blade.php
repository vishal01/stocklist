@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2>The easiest Way to buy and sell Stocks</h2>

                    <p>Stock Analysis and screening Tool For Investors In India</p>

                    <div class="serachautocomplete">
                        <!-- <input class="search form-control" id="compnylist" type="text"> -->
                        <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
                        <select class="livesearch form-control" name="livesearch"></select>
                        <input type="hidden" id="compid">
                    </div>

                </div>
            </div>

            <br/>

            <div class="detailblock" style="display: none;">
                <div class="card">
                   
                         <div class="card-header">Tech Mahindra LTD</div>

                         <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 blocks-marketcap">
                                    <span>Market Cap</span>
                                    <span class="price">&#x20b9;12334</span>
                                </div>
                                <div class="col-md-4 blocks-dividen">
                                    <span>Dividen Yield </span>
                                    <span class="price">77%</span>
                                </div>
                                <div class="col-md-3 blocks-dept">
                                    <span>Debt Equality</span>
                                    <span class="price">67%</span>
                                </div>
                            </div>
                            <br/>

                            <div class="row">
                                <div class="col-md-3 currentprice">
                                    <span>Current Price</span>
                                    <span class="price">&#x20b9;145</span>
                                </div>
                                <div class="col-md-4 roce">
                                    <span>ROCE</span>
                                    <span class="price">33%</span>
                                </div>
                                <div class="col-md-3 eps">
                                    <span>EPS</span>
                                    <span class="price">&#x20b9;567</span>
                                </div>
                            </div><br/>

                            <div class="row">
                                <div class="col-md-3 stockpe">
                                    <span>Stock P/E</span>
                                    <span class="price">7.16%</span>
                                </div>

                                <div class="col-md-3 roe">
                                    <span>ROE</span>
                                    <span class="price">33%</span>
                                </div>

                                <div class="col-md-3 reserv">
                                    <span>Resserves</span>
                                    <span class="price">&#x20b9;456</span>
                                </div>
                            </div><br/>

                            <div class="row">
                                <div class="col-md-3 debt">
                                    <span>Debt</span>
                                    <span class="price">&#x20b9;127</span>
                                </div>
                            </div>


                         </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Craysol Group -->
