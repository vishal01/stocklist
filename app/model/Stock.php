<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stocks';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'current_marketprice',
        'marketcap',
        'stockpe',
        'dividen',
        'roce',
        'roe_prev',
        'debtequity',
        'eps',
        'reserve',
        'debt',
        'created_at',
        'updated_at'
    ];
}
