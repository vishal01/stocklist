 var path = "listcompanies";
    // $('#compnylist').typeahead({
    //     source:  function (query, process) {
    //     return $.get(path, { query: query }, function (data) {
    //             return process(data);
    //         });
    //     }
    // });


    $('.livesearch').select2({
        placeholder: 'Select movie',
        ajax: {
            url: 'listcompanies',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true,
             tags: true,
        }
    });


$('body').find('.livesearch').change(function(){
    get_companydetails();
})

    function get_companydetails(){
        var selected = $('body').find('.select2-selection__rendered').text();
        $.ajax({
            url: 'companydata',
            data :{
                compname :selected
            },
            success : function(results){
                console.log(results[0].name);
                $('body').find('.detailblock .card-header').text(results[0].name);
                $('body').find('.detailblock .blocks-marketcap .price').text('₹'+results[0].marketcap);
                $('body').find('.detailblock .blocks-dividen .price').text(results[0].dividen+'%');
                $('body').find('.detailblock .blocks-dept .price').text(results[0].debt+'%');
                
                $('body').find('.detailblock .currentprice .price').text('₹'+results[0].current_marketprice);
                $('body').find('.detailblock .roce .price').text(results[0].roce+'%');
                $('body').find('.detailblock .eps .price').text('₹'+results[0].eps);
                
                $('body').find('.detailblock .stockpe .price').text('₹'+results[0].stockpe+'%');
                $('body').find('.detailblock .roe .price').text('₹'+results[0].roe_prev+'%');
                
                $('body').find('.detailblock .reserv .price').text('₹'+results[0].reserve);
                $('body').find('.detailblock .debt .price').text('₹'+results[0].debt);


                $('body').find('.detailblock').show();
            }
        })
    }


    //  $("#compnylist").autocomplete({
    //     source: function(request, response) {
    //         $.ajax({
    //             url: path,
    //             dataType: "json",
    //             data: {
    //                 term : request.term
    //             },
    //             success: function(data) {
    //                 response(data);
                   
    //             }
    //         });
    //     },
    //     minLength: 3,
       
    // });




